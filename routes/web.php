<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function () {
    Route::get('/', 'IndexController@index')->name('home');
    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::post('/settings/pushbullet', 'SettingsController@savePushbulletSettings')->name('settings.pushbullet');
    Route::post('/settings/fitmanager', 'SettingsController@saveFitmanagerSettings')->name('settings.fitmanager');
    Route::get('/settings/fitmanager/logout', 'SettingsController@fitmanagerLogout')->name('settings.fitmanager.logout');
    Route::get('/settings/google/code', 'SettingsController@googleCode')->name('settings.google.code');
    Route::get('/settings/google/logout', 'SettingsController@googleLogout')->name('settings.google.logout');
    Route::post('/settings/google/calendar', 'SettingsController@googleSaveCalendar')->name('settings.google.calendar');
    Route::post('/settings/password', 'SettingsController@updatePassword')->name('settings.password.update');

    Route::get('/events/register/{id}', 'IndexController@register')->name('event.register');

    Route::get('/rules', 'AutomicRulesController@index')->name('automic_rules.index');
    Route::post('/rules', 'AutomicRulesController@createRule')->name('automic_rules.index');
    Route::get('/rules/delete/{id}', 'AutomicRulesController@delete')->name('automic_rules.delete');

    Route::get('/calendar', 'IndexController@calendar')->name('calendar');
});
Auth::routes(['register' => false]);
