<?php


namespace App\Http\Controllers;


use App\Services\Exceptions\InvalidCredentialsException;
use App\Services\FitManager;
use App\Services\Google;
use App\Services\Pushbullet;
use GuzzleHttp\Exception\ClientException;
use http\Exception\InvalidArgumentException;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SettingsController extends Controller
{
    use ValidatesRequests;

    public function index()
    {
        $google = new Google;
        $calendars = $google->getCalendars();
        return view('settings.index', [
            'fitmanager_token' => auth()->user()->tokens()->where('name', 'fitmanager.token')->first(),
            'pushbullet_token' => auth()->user()->tokens()->where('name', 'pushbullet.token')->first(),
            'pushbullet_idens' => json_decode(auth()->user()->tokens()->where('name', 'pushbullet.idens')->first()->value ?? '[]'),
            'google_url' => $google->createLoginUrl(),
            'calendars' => $calendars,
            'selected_calendar_id' => auth()->user()->tokens()->where('name', 'google.calendar_id')->first()->value ?? null,
            'google_token' => auth()->user()->tokens()->where('name', 'google.access_token')->first(),
        ]);
    }

    public function googleSaveCalendar(Request $request)
    {
        $this->validate($request, [
            'calendar_id' => ['required'],
        ]);
        auth()->user()->tokens()->where('name', 'google.calendar_id')->delete();
        auth()->user()->tokens()->create([
            'name' => 'google.calendar_id',
            'value' => $request->get('calendar_id'),
        ]);
        return redirect()->back()->with('success', 'Agenda voorkeur is opgeslagen.');
    }

    public function saveFitmanagerSettings(Request $request)
    {
        $this->validate($request, [
            'fitmanager_email' => ['required', 'email'],
            'fitmanager_password' => ['required'],
        ]);
        try {
            $response = (new FitManager())->login($request->get('fitmanager_email'), $request->get('fitmanager_password'));
            $request->user()->tokens()->create([
                'name' => 'fitmanager.token',
                'value' => $response['data']['attributes']['token'],
            ]);
        } catch (InvalidCredentialsException $e) {
            return redirect()->back()->withErrors(['credentials' => 'Deze gegevens zijn onjuist.']);
        }
        return redirect()->back()->with('success', 'Ingelogd bij FitManager');
    }

    public function fitmanagerLogout()
    {
        auth()->user()->tokens()->where('name', 'fitmanager.token')->delete();
        return redirect()->back()->with('success', 'Uitgelogd bij FitManager');
    }

    public function googleCode(Request $request)
    {
        $this->validate($request, ['code' => ['required']]);
        try {
            $tokens = (new Google())->fetchTokens($request->get('code'));
            auth()->user()->tokens()->create([
                'value' => $tokens['access_token'],
                'name' => 'google.access_token',
            ]);
            auth()->user()->tokens()->create([
                'name' => 'google.refresh_token',
                'value' => $tokens['refresh_token'],
            ]);
            auth()->user()->tokens()->create([
                'name' => 'google.calendar_id',
                'value' => (new Google())->getCalendars()['items'][0]['id'],
            ]);
        } catch (ClientException $e) {
            return redirect(route('settings.index'))->with('danger', 'Er is iets fout gegaan probeer het opnieuw');
        }
        return redirect(route('settings.index'))->with('success', 'Ingelogd bij Google');
    }

    public function googleLogout()
    {
        auth()->user()->tokens()->where('name', 'google.access_token')->delete();
        auth()->user()->tokens()->where('name', 'google.refresh_token')->delete();
        auth()->user()->tokens()->where('name', 'google.calendar_id')->delete();

        return redirect()->back()->with('success', 'Uitlogd bij Google');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'password' => ['required', 'min:6', 'confirmed'],
            'old_password' => [function ($attribute, $value, $fail) {
                if (!password_verify($value, auth()->user()->password)) {
                    $fail('Oude wachtwoord is onjuist.');
                }
            }]
        ]);
        auth()->user()->update([
            'password' => bcrypt($request->get('password'))
        ]);

        return redirect()->back()->with('success', 'Wachtwoord is opgeslagen');
    }

    public function savePushbulletSettings(Request $request)
    {
        $this->validate($request, [
            'pushbullet_api_token' => [function ($attribute, $value, $fail) {
                if ($value === null) return;
                try {
                    Pushbullet::getDevices($value);
                } catch (InvalidCredentialsException $e) {
                    $fail('Deze api code is fout!');
                }
            }],
            'pushbullet_device_iden' => ['array'],
        ]);
        if ($request->has('pushbullet_api_token')) {
            auth()->user()->tokens()->where('name', 'pushbullet.token')->delete();
            if ($request->get('pushbullet_api_token') !== null) {
                auth()->user()->tokens()->create([
                    'name' => 'pushbullet.token',
                    'value' => $request->get('pushbullet_api_token')
                ]);
            }
        }
        if ($request->has('pushbullet_device_iden')) {
            auth()->user()->tokens()->where('name', 'pushbullet.idens')->delete();
            $value = $request->get('pushbullet_device_iden');
            if ($value !== null && count($value) > 0) {
                auth()->user()->tokens()->create([
                    'name' => 'pushbullet.idens',
                    'value' => json_encode($value)
                ]);
            }
        }
        return redirect()->back()->with('success', 'Pushbullet token is opgeslagen.');
    }
}
