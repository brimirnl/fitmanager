<?php

namespace App\Http\Controllers;

use App\Services\Exceptions\NoEventFoundException;
use App\Services\FitManager;
use App\Services\Google;
use Carbon\Carbon;

class IndexController extends Controller
{
    public function index()
    {
        $view = view('index', [
            'events' => $this->getEvents(),
            'errors' => auth()->user()->errors,
        ]);
        auth()->user()->errors()->delete();
        return $view;
    }

    public function register($id)
    {
        $errors = [];
        try {
            $fitmanager = new FitManager();
            $data = $fitmanager->getActivity($id);
            $start = Carbon::createFromFormat('Y-m-d\TH:i:s', $data['data']['attributes']['date'], new \DateTimeZone('Europe/Amsterdam'));
            $end = Carbon::createFromFormat('Y-m-d\TH:i:s', $data['data']['attributes']['enddate'], new \DateTimeZone('Europe/Amsterdam'));
            $fitmanager->signUp($data['data']['id']);
            $title = 'Sporten: ' . $data['data']['attributes']['title'];

            $google = new Google();
            try {
                $google->findEvent($title, $start, $end);
            } catch (NoEventFoundException $e) {
                $google->createEvent($title, $data['data']['attributes']['location']['title'], $start, $end);
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        $redirect = redirect()->back();
        if (count($errors) > 0) {
            $redirect->with('danger', 'Er is isets fout gegaan... ' . json_encode($errors));
        } else {
            $redirect->with('success', 'Je bent aangemeld voor deze les.');
        }

        return $redirect;
    }

    public function calendar()
    {
        $start = Carbon::now()->startOfWeek(1);
        if (now()->dayOfWeek >= 6 || now()->dayOfWeek === 0) {
            $start->addWeek();
        }
        try {
            $googleEvents = (new Google())->getEventsBetween($start, $start->clone()->addWeek())
                ?? ['items' => []];
        } catch (\Exception $E) {
            $googleEvents = ['items' => []];
        }
        return view('calendar', [
            'events' => $this->getEvents(),
            'googleEvents' => $googleEvents
        ]);
    }

    private function getEvents()
    {
        $fitmanager = new FitManager();
        try {
            $events = $fitmanager->getAllEvents();
            $ids = collect($fitmanager->getPlannedEvents()['data'])->pluck('id')->toArray();
            foreach ($events['data'] as &$event) {
                $event['joined'] = in_array($event['id'], $ids, true);
                unset($event);
            }
        } catch (\Exception $e) {
            $events = ['data' => []];
        }
        return $events;
    }
}
