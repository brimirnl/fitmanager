<?php


namespace App\Http\Controllers;


use App\Services\FitManager;

class EventController extends Controller
{
    public function info($id)
    {
        return (new FitManager())->getActivity($id);
    }
}
