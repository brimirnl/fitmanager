<?php

namespace App\Http\Controllers;

use App\Services\FitManager;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class AutomicRulesController extends Controller
{
    use ValidatesRequests;

    public function index()
    {
        try {
            $events = (new FitManager())->getAllEvents();
        } catch (\Exception $e) {
            $events = ['data' => []];
        }
        return view('automic_rules.index', [
            'events' => $events,
        ]);
    }

    public function createRule(Request $request)
    {
        $this->validate($request, [
            'day'   => ['required', 'min:0', 'max:6'],
            'time'  => ['required', 'regex:/^\d{2}:\d{2}$/'],
            'title' => ['required'],
        ]);

        auth()->user()->automicRules()->create([
            'day'   => $request->get('day'),
            'time'  => $request->get('time'),
            'title' => $request->get('title'),
        ]);

        return redirect()->back()->with('success', 'Regel is aangemaakt!');
    }

    public function delete($id)
    {
        auth()->user()->automicRules()->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Regel is verwijderd.');
    }
}