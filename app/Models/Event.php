<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Event extends Model
{
    protected $fillable = [
        'start',
        'end',
        'user_id',
        'fitmanager_id',
    ];
    protected $dates = [
        'start',
        'end'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
