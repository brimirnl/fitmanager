<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutomicRule extends Model
{
    protected $fillable = [
        'day',
        'time',
        'title',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
