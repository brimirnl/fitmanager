<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
    protected $fillable = [
        'user_id',
        'message',
        'stacktrace',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
