<?php


namespace App\Services;


use App\Models\Token;
use App\Models\User;
use App\Services\Exceptions\NoEventFoundException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\RequestInterface;

class Google
{
    private $client;
    private $calendar_id;

    public function __construct(User $user = null)
    {
        $options = ['base_uri' => 'https://www.googleapis.com/'];
        $user = $user ?? auth()->user();

        $this->calendar_id = $user->tokens()->where('name', 'google.calendar_id')->first()->value ?? null;

        if ($user !== null) {
            $options['handler'] = new HandlerStack();
            $options['handler']->setHandler(new CurlHandler());
            $options['handler']->push(function ($handler) use ($user) {
                return function (RequestInterface $request, $options) use ($handler, $user) {
                    $token = $user->tokens()->where('name', 'google.access_token')->first()->value ?? null;
                    $refresh_token = $user->tokens()->where('name', 'google.refresh_token')->first()->value ?? null;
                    if ($token === null) return $handler($request, $options);

                    $request = $request->withHeader('authorization', 'Bearer ' . $token);

                    $response = $handler($request, $options);
                    $data = json_decode((clone $response)->wait()->getBody(), true);
                    if (!is_array($data)) return $response;

                    if (($data['error']['message'] ?? '') === 'Invalid Credentials') {
                        $user->tokens()->where('name', 'google.access_token')->delete();
                        if ($refresh_token === null) return $response;

                        $response = (new Client())->post('https://www.googleapis.com/oauth2/v4/token', [
                            RequestOptions::FORM_PARAMS => [
                                'client_id' => env('GOOGLE_CLIENT_ID'),
                                'client_secret' => env('GOOGLE_CLIENT_SECRET'),
                                'redirect_uri' => $this->getRedirectUri(),
                                'grant_type' => 'refresh_token',
                                'refresh_token' => $refresh_token,
                            ],
                        ]);
                        $data = json_decode($response->getBody()->__toString(), true);
                        if (!is_array($data)) return $response;
                        if (!isset($data['access_token'])) return $response;

                        $user->tokens()->create([
                            'name' => 'google.access_token',
                            'value' => $data['access_token'],
                        ]);
                        $request = $request->withHeader('authorization', 'Bearer ' . $data['access_token']);

                        return $handler($request, $options);
                    }
                    return $response;
                };
            });
        }
        $this->client = new Client($options);
    }

    public function createLoginUrl()
    {
        return 'https://accounts.google.com/o/oauth2/v2/auth?' . http_build_query([
                'scope' => 'https://www.googleapis.com/auth/calendar',
                'access_type' => 'offline',
                'include_granted_scopes' => 'true',
                'redirect_uri' => $this->getRedirectUri(),
                'response_type' => 'code',
                'client_id' => env('GOOGLE_CLIENT_ID'),
                'prompt' => 'consent',
            ]);
    }

    public function fetchTokens(string $code)
    {
        return json_decode($this->client->post('oauth2/v4/token', [
            RequestOptions::FORM_PARAMS => [
                'client_id' => env('GOOGLE_CLIENT_ID'),
                'client_secret' => env('GOOGLE_CLIENT_SECRET'),
                'redirect_uri' => $this->getRedirectUri(),
                'grant_type' => 'authorization_code',
                'code' => $code,
            ],
        ])->getBody(), true);
    }

    public function findEvent(string $title, Carbon $start, Carbon $end)
    {
        $data = json_decode($this->client->get('calendar/v3/calendars/' . $this->calendar_id . '/events', [
            RequestOptions::QUERY => [
                'timeMin' => $start->format(Carbon::RFC3339),
                'timeMax' => $end->format(Carbon::RFC3339),
            ],
        ])->getBody(), true);
        foreach ($data['items'] as $event) {
            if ($event['summary'] === $title) return $event;
        }
        throw new NoEventFoundException('No event found');
    }

    public function createEvent(string $title, $location, Carbon $start, Carbon $end)
    {
        $this->client->post('calendar/v3/calendars/' . $this->calendar_id . '/events', [
            RequestOptions::JSON => [
                'summary' => $title,
                'location' => $location,
                'start' => [
                    'dateTime' => $start->format(Carbon::RFC3339),
                ],
                'end' => [
                    'dateTime' => $end->format(Carbon::RFC3339),
                ],
            ],
        ])->getBody();
    }

    public function getCalendars()
    {
        $data = json_decode($this->client->get('calendar/v3/users/me/calendarList')->getBody(), true);
        if (isset($data['errors'])) return ['items' => []];

        return $data;
    }

    public function getEventsBetween(Carbon $start, Carbon $end)
    {
        return json_decode($this->client->get('https://www.googleapis.com/calendar/v3/calendars/' . $this->calendar_id . '/events', [
            RequestOptions::QUERY => [
                'singleEvents' => 'true',
                'timeMax' => $end->format(Carbon::RFC3339),
                'timeMin' => $start->format(Carbon::RFC3339)
            ]
        ])->getBody()->__toString(),true);
    }

    private function getRedirectUri()
    {
        return route('settings.google.code');
    }
}
