<?php


namespace App\Services;


use App\Models\User;
use App\Services\Exceptions\InvalidCredentialsException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Response;
use Psr\Http\Message\RequestInterface;

class Pushbullet
{
    private $client;
    /** @var User|\Illuminate\Contracts\Auth\Authenticatable|null */
    private $user;

    public function __construct(User $user = null)
    {
        $settings = ['base_uri' => 'https://api.pushbullet.com/v2/'];
        $user = $user ?? auth()->user();
        $token = null;
        if ($user !== null) {
            $token = $user->tokens()->where('name', 'pushbullet.token')->first()->value ?? null;
        }
        $settings['handler'] = new HandlerStack();
        $settings['handler']->setHandler(new CurlHandler());
        $settings['handler']->push(function ($handler) use ($token) {
            return function (RequestInterface $request, array $options) use ($handler, $token) {
                if ($token !== null)
                    $request = $request->withHeader('Access-Token', $token);

                return $handler($request, $options);
            };
        });
        $this->client = new Client($settings);
        $this->user = $user;
    }

    public function sendPush(string $title, string $body)
    {
        if ($this->user === null) return;

        $idens = json_decode($this->user->tokens()->where('name', 'pushbullet.idens')->first()->value ?? '[]', true);
        foreach ($idens as $iden) {
            $this->client->post('pushes', [
                RequestOptions::FORM_PARAMS => [
                    'device_iden' => $iden,
                    'type' => 'note',
                    'title' => $title,
                    'body' => $body
                ]
            ]);
        }
    }

    public static function getDevices(string $apiKey)
    {
        try {
            $data = json_decode((new Client)
                ->get('https://api.pushbullet.com/v2/devices', [
                    RequestOptions::HEADERS => [
                        'Access-Token' => $apiKey
                    ]
                ])
                ->getBody()
                ->getContents(), true);
            $data = array_filter($data['devices'], function ($item) {
                return $item['active'];
            });

            return $data;
        } catch (ClientException $e) {
            if ($e->getCode() === Response::HTTP_UNAUTHORIZED) {
                throw new InvalidCredentialsException();
            }
            throw $e;
        }
    }
}
