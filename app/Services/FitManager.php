<?php


namespace App\Services;


use App\Models\User;
use App\Services\Exceptions\InvalidCredentialsException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpFoundation\Response;
use function GuzzleHttp\Psr7\parse_query;

class FitManager
{
    private $client;
    /** @var User|\Illuminate\Contracts\Auth\Authenticatable|null User */
    private $user;

    public function __construct(User $user = null)
    {
        $this->user = $user ?? auth()->user();
        $this->client = $this->createClient();
    }

    private function createClient()
    {
        $settings = ['base_uri' => 'https://club.fitmanager.com/'];
        $token = null;
        if ($this->user !== null) {
            $token = $this->user->tokens()->where('name', 'fitmanager.token')->first()->value ?? null;
        }

        $settings['handler'] = new HandlerStack();
        $settings['handler']->setHandler(new CurlHandler());
        $settings['handler']->push(function ($handler) use ($token) {
            return function (RequestInterface $request, array $options) use ($handler, $token) {
                if ($token !== null)
                    $request = $request->withHeader('Authorization', 'Bearer ' . $token);
                $request = $request->withUri(
                    $request->getUri()->withQuery(http_build_query(parse_query($request->getUri()->getQuery()) + [
                            'client_id' => 'mobile1_and_p',
                            'client_secret' => 'Tra7drEm8eV8kI8hat6juC2iV5Av0rayK9Of0dip',
                        ]))
                );

                return $handler($request, $options);
            };
        });
        return new Client($settings);
    }

    public function getActivity($id)
    {
        return json_decode($this->client->get('api/activities/' . $id)->getBody()->__toString(), true);
    }

    public function reauthenticate()
    {
        return json_decode($this->client->post('api/users/authenticate', [
            RequestOptions::QUERY => [
                'fresh' => 0,
            ],
        ])->getBody()->getContents(), true);
    }

    public function login(string $email, string $password)
    {
        try {
            return json_decode($this->client->post('api/users/authenticate', [
                RequestOptions::QUERY => [
                    'fresh' => '1',
                ],
                RequestOptions::AUTH => [$email, $password],
            ])->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() === Response::HTTP_UNAUTHORIZED) {
                throw new InvalidCredentialsException();
            }
            throw $e;
        }
    }

    public function getAllEvents()
    {
        $start = Carbon::now()->startOfWeek(1);
        if (now()->dayOfWeek >= 6 || now()->dayOfWeek === 0) {
            $start->addWeek();
        }
        $end = $start->clone()->addWeek();

        return json_decode($this->client->get('api/activities/schedule', [
            RequestOptions::QUERY => [
                'variant' => 'normal',
                'start_date' => $start->format('Y-m-d'),
                'end_date' => $end->format('Y-m-d'),
                'page' => '1',
                'pagesize' => '20',
            ],
        ])->getBody(), true);
    }

    public function getPlannedEvents()
    {
        $start = Carbon::now()->startOfWeek(1);
        if (now()->dayOfWeek >= 6 || now()->dayOfWeek === 0) {
            $start->addWeek();
        }
        $end = $start->clone()->addWeek();
        $data = json_decode($this->client->get('api/activities/planning', [
            RequestOptions::QUERY => [
                'variant' => 'normal',
                'start_date' => $start->format('Y-m-d'),
                'end_date' => $end->format('Y-m-d'),
                'page' => '1',
                'pagesize' => '20',
            ],
        ])->getBody(), true);

        if ($this->user === null) return $data;

        $this->user->events()->whereNotIn('fitmanager_id', array_column($data['data'], 'id'))->delete();

        foreach ($data['data'] as $item) {
            $this->user->events()->firstOrCreate([
                'start' => Carbon::createFromFormat('Y-m-d\TH:i:s', $item['attributes']['date']),
                'end' => Carbon::createFromFormat('Y-m-d\TH:i:s', $item['attributes']['enddate']),
                'fitmanager_id' => $item['id'],
            ]);
        }

        return $data;
    }

    public function signUp($id)
    {
        $this->client->put('/api/activities/' . $id . '/registration');
    }
}
