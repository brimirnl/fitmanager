<?php

namespace App\Console\Commands;

use App\Models\AutomicRule;
use App\Models\User;
use App\Services\Exceptions\NoEventFoundException;
use App\Services\FitManager;
use App\Services\Google;
use App\Services\Pushbullet;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SignUpForAutomicRules extends Command
{
    protected $signature = 'automic-rules:sign-up';

    protected $description = 'Command description';

    public function handle()
    {
        foreach (User::query()->get() as $user) {
            $this->handleUser($user);
        }
    }

    private function handleUser(User $user)
    {
        try {
            foreach ($user->automicRules as $rule) {
                $this->handleUserAndRule($user, $rule);
            }
        } catch (\Exception $e) {
            $user->errors()->create([
                'message' => $e->getMessage(),
                'stacktrace' => $e->getTraceAsString(),
            ]);
        }
    }

    private function handleUserAndRule(User $user, AutomicRule $rule)
    {
        $pushbullet = new Pushbullet($user);
        try {
            $fitmanager = new FitManager($user);
            $google = new Google($user);
            $events = $fitmanager->getAllEvents();
            foreach ($events['data'] as $event) {
                $start = Carbon::createFromFormat('Y-m-d\TH:i:s', $event['attributes']['date'], new \DateTimeZone('Europe/Amsterdam'));
                if ($event['attributes']['title'] === $rule->title && (int)$start->format('w') === $rule->day && $rule->time === $start->format('H:i')) {
                    $end = Carbon::createFromFormat('Y-m-d\TH:i:s', $event['attributes']['enddate']);
                    $title = 'Sporten: ' . $event['attributes']['title'];
                    $fitmanager->signUp($event['id']);
                    try {
                        $google->findEvent($title, $start, $end);
                    } catch (NoEventFoundException $e) {
                        $google->createEvent($title, $event['attributes']['location']['title'], $start, $end);
                    }
                }
            }
            // this is to save new events
            $fitmanager->getPlannedEvents();
            $pushbullet->sendPush('Automatisch aangemeld','Je bent automatisch aangemeld bij fitmanager. Sportze!');
        } catch (\Exception $e) {
            $user->errors()->create([
                'message' => $e->getMessage(),
                'stacktrace' => $e->getTraceAsString(),
            ]);
            $pushbullet->sendPush('ERROR!','Er is iets fout gegaan bij het aanmelden bij fitmanager!');
        }
    }
}
