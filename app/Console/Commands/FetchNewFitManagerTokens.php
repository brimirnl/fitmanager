<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\FitManager;
use Illuminate\Console\Command;

class FetchNewFitManagerTokens extends Command
{
    protected $signature = 'fitmanager:new-tokens';
    protected $description = 'Command description';

    public function handle()
    {
        $users = User::query()->whereHas('tokens', function ($builder) {
            return $builder->where('name', 'fitmanager.token');
        })->get();

        foreach ($users as $user) {
            $token = (new FitManager($user))->reauthenticate()['data']['attributes']['token'];
            $user->tokens()->where('name', 'fitmanager.token')->update(['value' => $token]);
        }
    }
}
