<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Services\FitManager;
use App\Services\Pushbullet;
use Illuminate\Console\Command;

class SendParticipantsAmount extends Command
{
    protected $signature = 'push:participants-amount';

    protected $description = 'Command description';

    public function handle()
    {
        /** @var Event[] $items */
        $items = Event::query()
            ->where('start', '<', now()->addMinutes(35))
            ->where('start', '>', now()->addMinutes(25))
            ->get();
        foreach ($items as $item) {
            $data = (new FitManager($item->user))
                ->getActivity($item->fitmanager_id);
            (new Pushbullet($item->user))
                ->sendPush(
                    sprintf('Aanmeldingen voor %s: %s', $data['data']['attributes']['title'], $data['data']['attributes']['participant_count']),
                    implode(', ', array_column($data['data']['relationships']['participants']['data']['info'], 'name'))
                );
        }
    }
}
