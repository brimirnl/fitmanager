<?php

namespace App\Console;

use App\Console\Commands\FetchNewFitManagerTokens;
use App\Console\Commands\SendParticipantsAmount;
use App\Console\Commands\SignUpForAutomicRules;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SignUpForAutomicRules::class,
        SendParticipantsAmount::class,
        FetchNewFitManagerTokens::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('automic-rules:sign-up')
            ->timezone('Europe/Amsterdam')
            ->saturdays()
            ->at('00:02');
        $schedule->command('push:participants-amount')
            ->timezone('Europe/Amsterdam')
            ->everyThirtyMinutes();
        $schedule->command('fitmanager:new-tokens')
            ->timezone('Europe/Amsterdam')
            ->fridays()
            ->at('00:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
