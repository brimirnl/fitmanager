@extends('layouts.simple')

@section('scripts')
    <link href='https://unpkg.com/@fullcalendar/core@4.3.1/main.min.css' rel='stylesheet'/>
    <link href='https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.css' rel='stylesheet'/>
    <link href='https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.css' rel='stylesheet'/>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src='https://unpkg.com/@fullcalendar/core@4.3.1/main.min.js'></script>
    <script src='https://unpkg.com/@fullcalendar/interaction@4.3.0/main.min.js'></script>
    <script src='https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.js'></script>
    <script src='https://unpkg.com/@fullcalendar/timegrid@4.3.0/main.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales/nl.js'></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/nl.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
    <script>window.events = {!! json_encode($events) !!}</script>
    <script>window.googleEvents = {!! json_encode($googleEvents) !!}</script>
    <style>
        .calendar-view > div {
            min-width: 900px;
        }

        .calendar-view {
            overflow: auto;
        }

        .popup {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: rgba(128, 128, 128, 0.50);
            z-index: 1;
        }

        .popup .card .card-body{
            overflow: auto;
        }
        .popup .card {
            margin: 25px;
        }
    </style>
@endsection
@section('content')
    <div id="calendar" class="container-fluid">
        <div class="filters">
            <select class="form-control" v-model="filters.title">
                <option :value="null">Allemaal</option>
                <option v-for="opt in titles" :value="opt" v-text="opt"></option>
            </select>
        </div>
        <div class="calendar-view">
            <div></div>
        </div>
        <div class="popup" v-if="showPopup">
            <div class="card card-sm">
                <div v-if="popup == null">
                    <div class="card-body">
                        loading...
                    </div>
                </div>
                <div v-else>
                    <div class="card-header">
                        <strong v-text="popup.attributes.title"></strong>
                        <div class="pull-left">
                            <i class="fas fa-times"></i>
                        </div>
                    </div>
                    <div class="card-body" :style="`max-height: ${popupHeight}px`">
                        <table class="table table-sm table-bordered">
                            <tr>
                                <td><strong>Titel:</strong></td>
                                <td v-text="popup.attributes.title"></td>
                            </tr>
                            <tr>
                                <td><strong>Locatie:</strong></td>
                                <td v-text="popup.attributes.location.title"></td>
                            </tr>
                            <tr>
                                <td><strong>Start:</strong></td>
                                <td v-text="popup.attributes.date"></td>
                            </tr>
                            <tr>
                                <td><strong>Einde:</strong></td>
                                <td v-text="popup.attributes.enddate"></td>
                            </tr>
                            <tr>
                                <td><strong>Aanmeldingen:</strong></td>
                                <td>
                                    <ul>
                                        <li v-for="item in popup.relationships.participants.data.info"
                                            v-text="item.name"></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        <div class="btn-group">
                            <a :href="`/events/register/${popup.id}`" class="btn btn-success"
                               onclick="return confirm('Weet je het zeker?');">Aanmelden</a>
                            <a class="btn btn-warning" href="#"
                               @click.prevent="closePopup">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        new Vue({
            el: '#calendar',
            data: {
                titles: [],
                calendar: null,
                filters: {
                    title: null,
                },
                showPopup: false,
                popup: null,
                popupHeight: 0
            },
            mounted() {
                window.addEventListener("resize", e => this.popupHeight = window.innerHeight - 100);
                this.popupHeight = window.innerHeight - 100;
                let self = this;
                this.calendar = new FullCalendar.Calendar(this.$el.querySelector('.calendar-view > div'), {
                    locale: 'nl',
                    plugins: ['dayGrid', 'timeGrid'],
                    defaultView: 'timeGridWeek',
                    defaultDate: this.getFilteredEvents()[0]['start'],
                    allDaySlot: false,
                    slotEventOverlap: false,
                    header: {
                        left: '',
                        center: 'title',
                        right: '',
                    },
                    minTime: '07:00:00',
                    events: this.getFilteredEvents(),
                    eventClick(e) {
                        if (Object.keys(e.event.extendedProps).length <= 0) return;

                        self.popup = null;
                        self.showPopup = true;
                        axios.get(`/api/get-info/${e.event.extendedProps.id}`)
                            .then(({data: {data}}) => self.popup = data);

                        window.scrollTo(0, 0);
                    }
                });
                this.calendar.render();
            },
            watch: {
                filters: {
                    deep: true,
                    handler() {
                        this.calendar.getEvents().forEach(i => i.remove());
                        this.getFilteredEvents().forEach(i => this.calendar.addEvent(i));
                        this.calendar.render();
                    }
                }
            },
            methods: {
                closePopup() {
                    this.showPopup = false;
                    this.popup = null;
                },
                getFilteredEvents() {
                    let events = [];
                    window.events.data.forEach(item => {
                        if (this.titles.indexOf(item.attributes.title) < 0)
                            this.titles.push(item.attributes.title);

                        if (this.filters.title !== null && this.filters.title !== item.attributes.title) return;
                        let [bg, border, text] = this.getColor(item);
                        events.push({
                            title: item.attributes.title,
                            start: item.attributes.date,
                            end: item.attributes.enddate,
                            backgroundColor: bg,
                            borderColor: border,
                            textColor: text,
                            extendedProps: item
                        });
                    });
                    window.googleEvents.items.forEach(item => {
                        events.push({
                            title: item.summary,
                            start: item.start.dateTime,
                            end: item.end.dateTime,
                            backgroundColor: '#e2e3e5',
                            borderColor: '#d6d8db',
                            textColor: '#383d41',
                        });
                    });
                    return events;
                },
                getColor(item) {
                    if (item.joined)
                        return ['#d4edda', '#c3e6cb', '#155724'];
                    if (item.attributes.cancelled)
                        return ['#f8d7da', '#f5c6cb', '#721c24'];
                    if (item.attributes.free_spots === 0)
                        return ['#fff3cd', '#ffeeba', '#856404'];
                    return ['#cce5ff', '#b8daff', '#004085'];
                }
            }
        })
    </script>
@endsection
