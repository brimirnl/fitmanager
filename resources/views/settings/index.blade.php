@extends('layouts.simple')

@section('content')
    <div class="container">
        @if($errors->count() > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $er)
                        <li>{{ $er }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h1>Settings</h1>
        <h2>FitManager</h2>
        @if($fitmanager_token === null)
            <div class="alert alert-warning">
                <p>Momenteel ben je niet ingelogd bij FitManager. Vul het formulier in om in te loggen.</p>
                <form method="post" action="{{ route('settings.fitmanager') }}">
                    {{ csrf_field() }}
                    <label>Email:</label>
                    <input type="text" class="form-control" name="fitmanager_email">
                    <label>Password:</label>
                    <input type="password" class="form-control" name="fitmanager_password">
                    <button type="submit" class="btn btn-success">Login</button>
                </form>
            </div>
        @else
            <p>Ingelogd bij FitManager.</p>
            <a href="{{ route('settings.fitmanager.logout') }}" class="btn btn-warning">Uitloggen</a>
        @endif
        <hr>
        <hr>
        <h2>Google</h2>
        @if($google_token === null)
            <div class="alert alert-warning">
                <p>Momenteel ben je niet ingelogd bij Google. Klik op de link hieronder om te authenticeren.</p>
                <a href="{{ $google_url }}" class="btn btn-success">Inloggen bij Google</a>
            </div>
        @else
            <p>Ingelogd bij Google.</p>
            <a href="{{ route('settings.google.logout') }}" class="btn btn-warning">Uitloggen</a><br>
            <br>
            <p>Selecteer agenda waarin de events opgeslagen moeten worden:</p>
            <form method="post" action="{{ route('settings.google.calendar') }}">
                {{ csrf_field() }}
                <select class="form-control" name="calendar_id">
                    @foreach($calendars['items'] as $item)
                        <option
                            {{ $selected_calendar_id == $item['id'] ? 'selected' : '' }} value="{{ $item['id'] }}">{{$item['summary']}}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn btn-success">Opslaan</button>
            </form>
        @endif
        <hr>
        <hr>
        <h2>Pushbullet</h2>
        <p>Hier kan je je pushbullet token opgeven om meldingen te ontvangen.</p>
        <form method="post" action="{{ route('settings.pushbullet') }}">
            {{ csrf_field() }}
            <label>Pushbullet token</label>
            <input type="text" value="{{ $pushbullet_token->value ?? '' }}" name="pushbullet_api_token"
                   class="form-control">

            @if($pushbullet_token !== null)
                <select name="pushbullet_device_iden[]" multiple class="form-control">
                    @foreach(\App\Services\Pushbullet::getDevices($pushbullet_token->value) as $item)
                        <option @if(in_array($item['iden'], $pushbullet_idens, true)) selected @endif value="{{ $item['iden'] }}">{{ $item['nickname'] }}</option>
                    @endforeach
                </select>
            @endif

            <button type="submit" class="btn btn-success">Opslaan</button>
        </form>
        <hr>
        <hr>
        <h2>Wachtwoord</h2>
        <p>Hier kan je je wachtwoord wijzigen</p>
        <form method="post" action="{{ route('settings.password.update') }}">
            {{ csrf_field() }}
            <label>Oude wachtwoord</label>
            <input type="password" name="old_password" class="form-control">
            <label>Nieuwe wachtwoord</label>
            <input type="password" name="password" class="form-control">
            <label>Nieuwe wachtwoord</label>
            <input type="password" name="password_confirmation" class="form-control">
            <button type="submit" class="btn btn-success">Opslaan</button>
        </form>
    </div>
@endsection
