@extends('layouts.simple')

@section('content')
    <div class="container">
        @if($errors->count() > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $er)
                        <li>{{ $er }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <br><br>
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>Les</th>
                    <th>Dag</th>
                    <th>Starttijd</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach(auth()->user()->automicRules as $rule)
                    <tr>
                        <td>{{ $rule->title }}</td>
                        <td>{{ now()->startOfWeek($rule->day)->format('l') }}</td>
                        <td>{{ $rule->time }}</td>
                        <td>
                            <a href="{{ route('automic_rules.delete',['id' => $rule->id]) }}" class="text-danger" onclick="return confirm('Weet je het zeker?')">Verwijderen</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            @foreach($events['data'] as $event)
                <div class="my-2 col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">{{ $event['attributes']['title'] }}</div>
                        <div class="card-body">
                            Datum: {{ \Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s',$event['attributes']['date'])->format('l') }}
                            <br>
                            Start: {{ \Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s',$event['attributes']['date'])->format('H:i') }}
                            <br>
                            Einde: {{ \Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s',$event['attributes']['enddate'])->format('H:i') }}
                            <br>
                            Locatie: {{ $event['attributes']['location']['title'] }}
                        </div>
                        <div class="card-footer">
                            <form action="{{ route('automic_rules.index') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="title" value="{{ $event['attributes']['title'] }}">
                                <input type="hidden" name="time"
                                       value="{{ \Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s',$event['attributes']['date'])->format('H:i') }}">
                                <input type="hidden" name="day"
                                       value="{{ \Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s',$event['attributes']['date'])->format('w') }}">
                                <button type="submit" class="btn btn-success">Regel aanmaken</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
