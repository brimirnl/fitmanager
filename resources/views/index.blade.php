@extends('layouts.simple')

@section('content')
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/nl.js"></script>
    <style>
        .fade-enter-active,
        .fade-leave-active {
            transition: opacity 300ms;
        }

        .fade-enter,
        .fade-leave-to {
            opacity: 0;
        }

        .fade-leave,
        .fade-enter-to {
            opacity: 1;
        }
    </style>
    <div class="container">
        @if($errors->count() > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors as $err)
                        <li>{{ $err->created_at->format('d-m-Y H:i:s') }} - {{ $err->message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div id="events">
            <div class="row py-4">
                <div class="col-md-6">
                    <select class="form-control" v-model="filters.day">
                        <option :value="null">Alle dagen</option>
                        <option v-for="day in days" :value="day" v-text="day"></option>
                    </select>
                </div>
                <div class="col-md-6">
                    <select class="form-control" v-model="filters.title">
                        <option :value="null">Allemaal</option>
                        <option v-for="title in titles" :value="title" v-text="title"></option>
                    </select>
                </div>
            </div>
            <transition-group name="fade" tag="div" class="row">
                <div class="col-md-4 my-2" v-for="event in filteredEvents" v-show="shouldShow(event)" :key="event.id">
                    <div class="card">
                        <div class="card-header" :class="background(event)">
                            <b v-text="event.attributes.title"></b>
                            <span class="float-right"
                                  v-text="`${event.attributes.total_spots - event.attributes.free_spots}/${event.attributes.total_spots}`"></span>
                        </div>
                        <div class="card-body">
                            <table class="table table-sm">
                                <tr>
                                    <td><b>Datum:</b></td>
                                    <td v-text="day(event.attributes.date)"></td>
                                </tr>
                                <tr>
                                    <td><b>Start:</b></td>
                                    <td v-text="time(event.attributes.date)"></td>
                                </tr>
                                <tr>
                                    <td><b>Einde:</b></td>
                                    <td v-text="time(event.attributes.enddate)"></td>
                                </tr>
                                <tr>
                                    <td><b>Locatie:</b></td>
                                    <td v-text="event.attributes.location.title"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="card-footer" v-if="event.attributes.free_spots > 0 && !event.joined">
                            <a :href="`/events/register/${event.id}`" class="btn btn-success btn-sm">Aanmelden</a>
                        </div>
                    </div>
                </div>
            </transition-group>
        </div>
    </div>

    <script>
        window.events = {!! json_encode($events['data']) !!};
        new Vue({
            el: '#events',
            data: {
                events: window.events,
                filters: {
                    day: null,
                    title: null
                }
            },
            computed: {
                days() {
                    let days = [];
                    this.events.forEach(i => {
                        let day = moment(i.attributes.date).format('dddd');
                        if (days.indexOf(day) === -1) days.push(day);
                    });
                    return days;
                },
                titles() {
                    let titles = [];
                    this.events.forEach(i => {
                        if (titles.indexOf(i.attributes.title) === -1) titles.push(i.attributes.title);
                    });
                    return titles;
                },
                filteredEvents() {
                    return this.events;
                }
            },
            methods: {
                day(date) {
                    return window.moment(date).format('dddd DD MMMM');
                },
                time(date) {
                    return window.moment(date).format('HH:mm');
                },
                background(event) {
                    if (event.attributes.free_spots <= 0) return 'bg-warning';
                    if (event.joined) return 'bg-success text-white';
                },
                shouldShow(event) {
                    if (this.filters.day !== null && moment(event.attributes.date).format('dddd') !== this.filters.day) return false;
                    if (this.filters.title !== null && this.filters.title !== event.attributes.title) return false;
                    return true;
                }
            }
        });
    </script>
@endsection
